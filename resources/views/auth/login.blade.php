<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login - {{ config('app.name') }}</title>

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="/css/app.css">
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-lg-5 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">
                                    <a href="/" class="text-dark text-decoration-none">{{ config('app.name') }}</a>
                                </h1>
                            </div>
                            <form class="user" action="/login" method="POST">
                                @csrf
                                {{-- username --}}
                                <div class="form-group">
                                    <div class="input-group rounded-0">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text rounded-0"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" class="form-control rounded-0" name="username" placeholder="Username" autocomplete="off" value="{{ old('username') }}">
                                    </div>
                                    @error('username')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                {{-- password --}}
                                <div class="form-group">
                                    <div class="input-group rounded-0">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text rounded-0"><i class="fas fa-lock"></i></span>
                                        </div>
                                        <input type="password" class="form-control rounded-0" name="password" placeholder="Password">
                                    </div>
                                    @error('password')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox small">
                                        <input type="checkbox" class="custom-control-input" id="customCheck">
                                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-block btn-lg rounded-0">
                                    Sign In <i class="fas fa-sign-in-alt fa-fw"></i>
                                </button>
                            </form>
                            {{-- <hr>
                            <div class="text-center">
                                <a class="small" href="/forgot/password">Lupa Password?</a>
                            </div> --}}
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="/js/app.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin-2.min.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    @if ( session('response') )
        {!! session('response') !!}
    @endif

</body>

</html>