@extends('layouts.admin')
@section('title', 'Pengaturan Umum')
@section('pengaturan', 'active-link')

@section('content')
    
<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/pengaturan/{{ $setting->id }}" method="post">
                    @csrf @method('PUT')

                    {{-- denda --}}
                    <div class="form-group row">
                        <label for="denda" class="col-sm-2 col-form-label">Denda</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('denda') is-invalid @enderror" name="denda" id="denda" autocomplete="off" value="{{ old('denda', $setting->denda) }}" placeholder="Denda Keterlambatan">
                            @error('denda')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- lama_pinjam --}}
                    <div class="form-group row">
                        <label for="lama_pinjam" class="col-sm-2 col-form-label">Lama Pinjam Buku / Hari</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('lama_pinjam') is-invalid @enderror" name="lama_pinjam" id="lama_pinjam" autocomplete="off" value="{{ old('lama_pinjam', $setting->lama_pinjam) }}" placeholder="Lama Peminjaman Buku">
                            @error('lama_pinjam')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <button class="btn btn-primary btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection