@extends('layouts.admin')
@section('title', 'Tambah Siswa')
@section('master', 'active-link')

@section('content')
    
<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <form action="/siswa" method="post">
                    @csrf

                    {{-- nis --}}
                    <div class="form-group row">
                        <label for="nis" class="col-sm-2 col-form-label">NIS</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('nis') is-invalid @enderror" name="nis" id="nis" autocomplete="off" placeholder="NIS" value="{{ old('nis') }}">
                            @error('nis')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- nama --}}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('nama') is-invalid @enderror" name="nama" id="nama" autocomplete="off" placeholder="Nama Siswa" value="{{ old('nama') }}">
                            @error('nama')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- jenis_kelamin --}}
                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            {{-- laki laki --}}
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="laki" name="jenis_kelamin" class="custom-control-input" {{ old('jenis_kelamin') == 'L' ? 'checked' : '' }} value="L">
                                <label class="custom-control-label" for="laki">L</label>
                            </div>
                            {{-- perempuan --}}
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="perempuan" name="jenis_kelamin" class="custom-control-input" {{ old('jenis_kelamin') == 'P' ? 'checked' : '' }} value="P">
                                <label class="custom-control-label" for="perempuan">P</label>
                            </div>
                            @error('jenis_kelamin')
                                <small class="text-danger d-block">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- alamat --}}
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea name="alamat" id="alamat" class="form-control rounded-0 @error('alamat') is-invalid @enderror" placeholder="Alamat">{{ old('alamat') }}</textarea>
                            @error('alamat')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <a href="/siswa" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection