@extends('layouts.admin')
@section('title', 'Data Siswa')
@section('master', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/siswa/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah Siswa
                </a>

                {{-- table --}}
                <div class="mt-4 table-responsive">
                    <table class="table table-striped table-bordered text-center w-100" id="table-siswa">
                        <thead>
                            <tr>
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th style="min-width: 170px">Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
<script>
    var table = $('#table-siswa').DataTable({
            processing: true,
            order: [],
            language: {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/siswa/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "nis"},
                {data: "nama"},
                {data: "jk"},
                {data: "alamat"},
                {data: "opsi", searchable: false, orderable: false},
            ]
        });
</script>
<script src="/js/delete.js"></script>
@if ( session('response') )
    {!! session('response') !!}
@endif
@endsection