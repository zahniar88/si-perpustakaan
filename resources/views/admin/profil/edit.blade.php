@extends('layouts.admin')
@section('title', 'Edit Profil')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <div class="alert alert-info mb-4">
                    Anda dapat mengubah foto profil akun di <a href="http://gravatar.com">gravatar</a> dengan cara mendaftar menggunakan email yang sama pada akun ini.
                </div>

                {{-- form --}}
                <form action="/profil" method="post">
                    @csrf @method('PUT')

                    {{-- nama --}}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('nama') is-invalid @enderror" name="nama" id="nama" autocomplete="off" placeholder="Nama Lengkap" value="{{ old('nama', Auth::user()->nama) }}">
                            @error('nama')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- email --}}
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('email') is-invalid @enderror" name="email" id="email" autocomplete="off" placeholder="Alamat Email" value="{{ old('email', Auth::user()->email) }}">
                            @error('email')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- username --}}
                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('username') is-invalid @enderror" name="username" id="username" autocomplete="off" placeholder="Username" value="{{ old('username', Auth::user()->username) }}">
                            @error('username')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- password --}}
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control rounded-0 @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
                            @error('password')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- password_confirmation --}}
                    <div class="form-group row">
                        <label for="password_confirmation" class="col-sm-2 col-form-label">Konfirmasi Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control rounded-0" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password">
                        </div>
                    </div>

                    <button class="btn btn-primary btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection