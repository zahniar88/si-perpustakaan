@extends('layouts.print')
@section('title', 'Laporan Pengadaan Buku')

@section('content')

<div class="container py-3">
    <h2 class="text-center mb-5">Laporan Pengadaan Buku</h2>
    <p class="mb-1">Per tanggal {{ Request::input('tanggal_awal') }} s/d {{ Request::input('tanggal_akhir') }}</p>

    {{-- table --}}
    <table class="table table-bordered table-striped text-center w-100">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Judul</th>
                <th>Asal Buku</th>
                <th>Keterangan</th>
                <th>Jumlah</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total = 0;
            @endphp
            @foreach ($procurements as $procurement)
                <tr>
                    <td>{{ $procurement->tanggal }}</td>
                    <td>{{ $procurement->book->judul }}</td>
                    <td>{{ $procurement->asal }}</td>
                    <td>{{ $procurement->keterangan }}</td>
                    <td>{{ $procurement->jumlah }}</td>
                </tr>
                @php
                    $total += $procurement->jumlah;
                @endphp
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total Buku</th>
                <th>{{ $total }}</th>
            </tr>
        </tfoot>
    </table>
    {{-- end table --}}

    <small class="d-block">Dicetak oleh : {{ Auth::user()->nama }}</small>
    <small>Pada tanggal : {{ date('d F Y') }}</small>
</div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            window.print();
        });
    </script>
@endsection