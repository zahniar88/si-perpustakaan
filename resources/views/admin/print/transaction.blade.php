@extends('layouts.print')
@section('title', 'Laporan Pengadaan Buku')

@section('content')

<div class="container py-3">
    <h2 class="text-center mb-5">Laporan Peminjaman Buku</h2>
    <p class="mb-1">Per tanggal {{ Request::input('tanggal_awal') }} s/d {{ Request::input('tanggal_akhir') }}</p>

    {{-- table --}}
    <table class="table table-bordered table-striped text-center w-100">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tanggal Pinjam</th>
                <th>Jatuh Tempo</th>
                <th>Nama Siswa</th>
                <th>Status</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transactions as $transaction)
            <tr>
                <td>{{ $transaction->kd_pinjam }}</td>
                <td>{{ $transaction->tanggal_pinjam }}</td>
                <td>{{ $transaction->tanggal_tempo }}</td>
                <td>{{ $transaction->student->nama }}</td>
                <td>{{ $transaction->status == 0 ? 'Kembali' : 'Pinjam' }}</td>
                <td>{{ $transaction->keterangan }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{-- end table --}}

    <small class="d-block">Dicetak oleh : {{ Auth::user()->nama }}</small>
    <small>Pada tanggal : {{ date('d F Y') }}</small>
</div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            window.print();
        });
    </script>
@endsection