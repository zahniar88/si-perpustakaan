@extends('layouts.admin')
@section('title', 'Tambah Rak')
@section('master', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/rak" method="post">
                    @csrf

                    {{-- rak --}}
                    <div class="form-group">
                        <label for="rak">Rak</label>
                        <input type="text" class="form-control rounded-0 @error('rak') is-invalid @enderror" name="rak" id="rak" autocomplete="off" placeholder="Nama Rak" value="{{ old('rak') }}">
                        @error('rak')
                            <small class="invalid-feedback">{{ $message }}</small>
                        @enderror
                    </div>

                    {{-- keterangan --}}
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control rounded-0 @error('keterangan') is-invalid @enderror" name="keterangan" id="keterangan" autocomplete="off" placeholder="Nama keterangan" value="{{ old('keterangan') }}">
                        @error('keterangan')
                            <small class="invalid-feedback">{{ $message }}</small>
                        @enderror
                    </div>

                    <a href="/rak" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection