@extends('layouts.admin')
@section('title', 'Data Rak')
@section('master', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/rak/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah Rak
                </a>

                {{-- table --}}
                <div class="mt-4 table-responsive">
                    <table class="table table-striped table-bordered w-100 text-center" id="table-rak">
                        <thead>
                            <tr>
                                <th>Rak</th>
                                <th>Keterangan</th>
                                <th style="max-width: 180px">Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    <script>
        var table = $('#table-rak').DataTable({
            processing: true,
            order: [],
            language: {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/rak/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "rak"},
                {data: "keterangan"},
                {data: "opsi", searchable: false, orderable: false},
            ]
        });
    </script>
    <script src="/js/delete.js"></script>
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection