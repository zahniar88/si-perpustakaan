@extends('layouts.admin')
@section('title', 'Ubah Penerbit')
@section('master', 'active-link')

@section('content')
    
<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/penerbit/{{ $publisher->id }}" method="post">
                    @csrf @method('PUT')

                    {{-- penerbit --}}
                    <div class="form-group">
                        <label for="penerbit">Penerbit</label>
                        <input type="text" class="form-control rounded-0 @error('penerbit') is-invalid @enderror" name="penerbit" id="penerbit" autocomplete="off" placeholder="Nama penerbit" value="{{ old('penerbit', $publisher->nama) }}">
                        @error('penerbit')
                            <small class="invalid-feedback">{{ $message }}</small>
                        @enderror
                    </div>

                    <a href="/penerbit" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection