@extends('layouts.admin')
@section('title', 'Tambah Pengguna')
@section('pengguna', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/pengguna" method="post">
                    @csrf

                    {{-- nama --}}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Nama Lengkap" autocomplete="off" value="{{ old('nama') }}">
                            @error('nama')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- email --}}
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('email') is-invalid @enderror" name="email" id="email" placeholder="Alamat Email" autocomplete="off"value="{{ old('email') }}">
                            @error('email')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- username --}}
                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('username') is-invalid @enderror" name="username" id="username" placeholder="Username" autocomplete="off" value="{{ old('username') }}">
                            @error('username')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- level --}}
                    <div class="form-group row">
                        <label for="level" class="col-sm-2 col-form-label">Level</label>
                        <div class="col-sm-10">
                            <select name="level" id="level" class="custom-select rounded-0 @error('level') is-invalid @enderror">
                                <option>Pilih Level</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}" {{ old('level') == $role->id ? 'selected' : '' }}>{{ $role->level }}</option>
                                @endforeach
                            </select>
                            @error('level')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- password --}}
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control rounded-0 @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" autocomplete="off">
                            @error('password')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- password_confirmation --}}
                    <div class="form-group row">
                        <label for="password_confirmation" class="col-sm-2 col-form-label">Konfirmasi Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control rounded-0" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password" autocomplete="off">
                        </div>
                    </div>

                    <a href="/pengguna" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection