@extends('layouts.admin')
@section('title', 'Data Pengguna')
@section('pengguna', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/pengguna/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah Pengguna
                </a>

                {{-- table --}}
                <div class="mt-4 table-responsive">
                    <table class="table table-striped table-bordered text-center w-100" id="table-pengguna">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Status</th>
                                <th>Level</th>
                                <th style="min-width: 180px">Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
<script>
    var table = $('#table-pengguna').DataTable({
            processing: true,
            order: [],
            language:  {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/pengguna/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "nama"},
                {data: "email"},
                {data: "username"},
                {data: "status"},
                {data: "level"},
                {data: "opsi", searchable: false, orderable: false},
            ]
        });
</script>
<script src="/js/delete.js"></script>
@if ( session('response') )
    {!! session('response') !!}
@endif
@endsection