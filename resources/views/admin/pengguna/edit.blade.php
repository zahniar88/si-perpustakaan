@extends('layouts.admin')
@section('title', 'Ubah Pengguna')
@section('pengguna', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/pengguna/{{ $user->id }}" method="post">
                    @csrf @method('PUT')

                    {{-- nama --}}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Nama Lengkap" autocomplete="off" value="{{ old('nama', $user->nama) }}">
                            @error('nama')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- email --}}
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('email') is-invalid @enderror" name="email" id="email" placeholder="Alamat Email" autocomplete="off"value="{{ old('email', $user->email) }}">
                            @error('email')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- username --}}
                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('username') is-invalid @enderror" name="username" id="username" placeholder="Username" autocomplete="off" value="{{ old('username', $user->username) }}">
                            @error('username')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- level --}}
                    <div class="form-group row">
                        <label for="level" class="col-sm-2 col-form-label">Level</label>
                        <div class="col-sm-10">
                            <select name="level" id="level" class="custom-select rounded-0 @error('level') is-invalid @enderror">
                                <option>Pilih Level</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}" {{ old('level', $user->role_id) == $role->id ? 'selected' : '' }}>{{ $role->level }}</option>
                                @endforeach
                            </select>
                            @error('level')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- status --}}
                    <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <select name="status" id="status" class="custom-select rounded-0 @error('status') is-invalid @enderror">
                                <option>Pilih Satus</option>
                                <option value="1" {{ old('status', $user->status) == 1 ? 'selected' : '' }}>Aktif</option>
                                <option value="0" {{ old('status', $user->status) == 0 ? 'selected' : '' }}>Nonaktif</option>
                            </select>
                            @error('status')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <a href="/pengguna" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection