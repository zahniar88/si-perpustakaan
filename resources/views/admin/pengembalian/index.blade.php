@extends('layouts.admin')
@section('title', 'Data Pengembalian')
@section('transaksi', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/pengembalian/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah pengembalian
                </a>

                {{-- table --}}
                <div class="mt-4 table-responsive">
                    <table class="table table-striped table-bordered text-center w-100" id="table-pengembalian">
                        <thead>
                            <tr>
                                <th>Tanggal kembali</th>
                                <th>ID Peminjaman</th>
                                <th>Siswa</th>
                                <th>Tanggal Pinjam</th>
                                <th>Jatuh Tempo</th>
                                <th>Denda</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
<script>
    var table = $('#table-pengembalian').DataTable({
            processing: true,
            order: [],
            language: {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/pengembalian/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "tgl_kembali"},
                {data: "id"},
                {data: "siswa"},
                {data: "tgl_pinjam"},
                {data: "tgl_tempo"},
                {data: "denda"},
            ]
        });
</script>
<script src="/js/delete.js"></script>
@if ( session('response') )
    {!! session('response') !!}
@endif
@endsection