@extends('layouts.admin')
@section('title', 'Tambah Pengembalian')
@section('transaksi', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <form action="/pengembalian" method="post">
                    @csrf

                    {{-- id --}}
                    <div class="form-group row">
                        <label for="id" class="col-md-2 col-form-label">ID Pinjam / Nama Siswa</label>
                        <div class="col-md-6">
                            <select name="id" id="siswa" class="custom-select w-100">
                                <option value="">Pilih Data</option>
                                @foreach ($transactions as $transaction)
                                    <option value="{{ $transaction->id }}">{{ $transaction->kd_pinjam }} - {{ $transaction->student->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    {{-- tgl_pinjam --}}
                    <div class="form-group row">
                        <label for="tgl_pinjam" class="col-md-2 col-form-label">Tanggal Pinjam</label>
                        <div class="col-md-4">
                            <input type="text" readonly class="form-control rounded-0" name="tgl_pinjam" id="tgl_pinjam" value="">
                        </div>
                    </div>

                    {{-- tgl_kembali --}}
                    <div class="form-group row">
                        <label for="tgl_kembali" class="col-md-2 col-form-label">Tanggal Kembali</label>
                        <div class="col-md-4">
                            <input type="text" readonly class="form-control rounded-0" name="tgl_kembali" id="tgl_kembali" value="">
                        </div>
                    </div>

                    {{-- terlambat --}}
                    <div class="form-group row">
                        <label for="terlambat" class="col-md-2 col-form-label">Terlambat / Hari</label>
                        <div class="col-md-4">
                            <input type="text" readonly class="form-control rounded-0" name="terlambat" id="terlambat" value="">
                        </div>
                    </div>

                    {{-- denda --}}
                    <div class="form-group row">
                        <label for="denda" class="col-md-2 col-form-label">Denda</label>
                        <div class="col-md-4">
                            <input type="text" readonly class="form-control rounded-0" name="denda" id="denda" value="">
                        </div>
                    </div>

                    {{-- table --}}
                    <div class="form-group row">
                        <label for="table" class="col-md-2 col-form-label">Buku</label>
                        <div class="col-md-10">
                            <div class="scroll-x">
                                <table class="table table-striped table-bordered w-100 text-center">
                                    <thead>
                                        <tr>
                                            <th>ID Buku</th>
                                            <th>Judul</th>
                                            <th>ISBN</th>
                                            <th>Pengarang</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody id="row-content"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <a href="/pengembalian" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-primary btn-sm rounded-0">
                        <i class="fas fa-sync"></i> Proses Transaksi 
                    </button>

                </form>

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    <script>
        $('#siswa').select2();

        $('body').on('change', '#siswa', function() {
            var id = $(this).val();

            if ( $(this).val() ) {
                // ambil denda
                $.ajax({
                    method: 'GET',
                    url: '/pengembalian/denda',
                    data: {'id': id}
                }).done(function(data) {
                    $('#tgl_pinjam').val(data.tanggal_pinjam);
                    $('#tgl_kembali').val(data.tanggal_tempo);
                    $('#terlambat').val(data.terlambat);
                    $('#denda').val(data.denda);
                });
                
                // ambil data buku
                $.ajax({
                    method: 'GET',
                    url: '/pengembalian/buku',
                    data: {'id': id}
                }).done(function(data) {
                    $('#row-content').html(data);
                });
            } else {
                $('#tgl_pinjam').val('');
                $('#tgl_kembali').val('');
                $('#terlambat').val('');
                $('#denda').val('');
                $('#row-content').html('');
            }

        });
    </script>
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection