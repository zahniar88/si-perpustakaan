@extends('layouts.admin')
@section('title', 'Data Peminjaman')
@section('transaksi', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/peminjaman/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah Peminjaman
                </a>

                {{-- table --}}
                <div class="mt-4 table-responsive">
                    <table class="table table-bordered table-striped text-center w-100" id="table-pinjaman">
                        <thead>
                            <tr>
                                <th>ID Pinjam</th>
                                <th>Tanggal Pinjam</th>
                                <th>Nama Siswa</th>
                                <th>Tempo</th>
                                <th>Terlambat</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
<script>
    var table = $('#table-pinjaman').DataTable({
            processing: true,
            order: [],
            language:  {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/peminjaman/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "id"},
                {data: "tanggal_pinjam"},
                {data: "nama_siswa"},
                {data: "tempo"},
                {data: "terlambat"},
                {data: "status"},
                {data: "keterangan"},
                {data: "opsi", searchable: false, orderable: false},
            ]
        });
</script>
<script src="/js/delete.js"></script>
@if ( session('response') )
    {!! session('response') !!}
@endif
@endsection