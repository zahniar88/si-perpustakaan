@extends('layouts.admin')
@section('title', 'Tambah Peminjaman')
@section('transaksi', 'active-link')

@section('content')

<div class="row">
    <form class="col-md-12" action="/peminjaman" method="POST">

        @csrf

        {{-- card 1 --}}
        <div class="card shadow rounded-0 border-0 mb-4 border-left-primary">
            <div class="card-body rounded-0">

                {{-- id --}}
                <div class="form-group row">
                    <label for="id" class="col-sm-2 col-form-label">ID</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control rounded-0" name="id" id="id" value="{{ $id }}" style="max-width: 220px">
                    </div>
                </div>

                {{-- tgl_pinjam --}}
                <div class="form-group row">
                    <label for="tgl_pinjam" class="col-sm-2 col-form-label">Tanggal Pinjam</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control rounded-0" name="tgl_pinjam" id="tgl_pinjam" value="{{ now()->isoFormat('DD-MM-YYYY') }}" style="max-width: 220px">
                    </div>
                </div>

                {{-- tgl_kembali --}}
                <div class="form-group row">
                    <label for="tgl_kembali" class="col-sm-2 col-form-label">Tanggal Kembali</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control rounded-0" name="tgl_kembali" id="tgl_kembali" value="{{ now()->addDay($lama_pinjam)->isoFormat('DD-MM-YYYY') }}" style="max-width: 220px">
                    </div>
                </div>

                {{-- siswa --}}
                <div class="form-group row">
                    <label for="siswa" class="col-sm-2 col-form-label">Siswa</label>
                    <div class="col-sm-10">
                        <select name="siswa" id="siswa" class="custom-select select2" style="max-width: 70%">
                            <option value="">Pilih Siswa</option>
                            @foreach ($students as $student)
                                <option value="{{ $student->id }}" {{ old('siswa') == $student->id ? 'selected' : '' }}>{{ $student->nama }} - {{ $student->nis }}</option>
                            @endforeach
                        </select>
                        @error('siswa')
                            <small class="text-danger d-block">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

            </div>
        </div>
        {{-- end card 1 --}}

        {{-- card 2 --}}
        <div class="card shadow rounded-0 border-0 mb-4 border-left-primary">
            <div class="card-body rounded-0">

                {{-- table --}}
                <div class="scroll-x">
                    <table class="table table-striped table-bordered w-100 mb-4">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>ISBN</th>
                                <th>Pengarang</th>
                                <th>Qty</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody id="row-content">
                            <tr>
                                <td style="max-width: 250px">
                                    <select name="buku[]" class="custom-select select2 buku" style="width: 100%">
                                        <option value="">Pilih Buku</option>
                                        @foreach ($books as $book)
                                            <option value="{{ $book->id }}">{{ $book->judul }}</option>
                                        @endforeach
                                    </select>
                                    @error('buku')
                                        <small class="text-danger d-block">{{ $message }}</small>
                                    @enderror
                                    @error('buku.*')
                                        <small class="text-danger d-block">{{ $message }}</small>
                                    @enderror
                                </td>
                                <td></td>
                                <td></td>
                                <td style="max-width: 40px; min-width: 40px;">
                                    <input type="text" class="form-control rounded-0" name="qty[]" autocomplete="off">
                                    @error('qty.*')
                                        <small class="text-danger d-block">{{ $message }}</small>
                                    @enderror
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>

                    <button class="btn btn-primary btn-sm rounded-0" id="add-buku">
                        <i class="fas fa-plus"></i> Tambah Buku
                    </button>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card 2 --}}

        {{-- card 3 --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- keterangan --}}
                <div class="form-group row">
                    <label for="keterangan" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea name="keterangan" id="keterangan" class="form-control rounded-0" placeholder="Keterangan">{{ old('keterangan') }}</textarea>
                        @error('keterangan')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <a href="/peminjaman" class="btn btn-dark btn-sm rounded-0">
                    <i class="fas fa-times"></i> Batal
                </a>
                <button class="btn btn-success btn-sm rounded-0">
                    <i class="fas fa-sync"></i> Proses Transaksi
                </button>

            </div>
        </div>
        {{-- end card 3 --}}

    </form>
</div>

@endsection

@section('js')
    <script>
        $('.select2').select2();

        // tambah buku
        var noBuku = 2;
        $('#add-buku').click(function(e) {
            e.preventDefault();
            var target = $('#row-content');
            var opsiBuku = $('.buku').eq(0).html();
            if ( noBuku <= 3 ) {
                var row = '';
                row = '<tr>' +
                            '<td style="max-width: 250px">' +
                                '<select name="buku[]" class="custom-select select2" style="width: 100%">' +
                                    opsiBuku +
                                '</select>' +
                            '</td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td style="max-width: 40px; min-width: 40px;">' +
                                '<input type="text" class="form-control rounded-0" name="qty[]" autocomplete="off">' +
                            '</td>' +
                            '<td style="max-width: 50px; min-width:50" class="text-center">' +
                                '<button class="btn btn-danger btn-sm rounded-0" id="remove-buku">' +
                                    '<i class="fas fa-times"></i>' +
                                '</button>' +
                            '</td>' +
                        '</tr>';
                target.append(row);
                $('.select2').select2();
                noBuku++;
            } else {
                return false;
            }
        });

        // hapus buku
        $('body').on('click', '#remove-buku', function(e) {
            e.preventDefault();
            var target = $(this).parent().parent();
            target.remove();
            noBuku--;
        });

        // on select event
        $('body').on('change', '#row-content select', function() {
            var id = $(this).val();
            var isbn = $(this).parent().parent().find('td:nth-child(2)');
            var pengarang = $(this).parent().parent().find('td:nth-child(3)');
            $.ajax({
                method: 'POST',
                url: '/peminjaman/buku',
                data: {'id': id}
            }).done(function(data) {
                if ( data ) {
                    isbn.text(data.isbn);
                    pengarang.text(data.pengarang);
                } else {
                    isbn.text('');
                    pengarang.text('');
                }
            });
        });
    </script>
@endsection