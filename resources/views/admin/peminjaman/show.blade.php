@extends('layouts.admin')
@section('title', 'Detail Peminjaman')
@section('transaksi', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0">
            <div class="card-body rounded-0">

                {{-- header --}}
                <div class="row justify-content-between">
                    <div class="col-md-4">
                        <h5 class="card-title text-dark mb-0">
                            <i class="fas fa-info-circle"></i> Detail
                        </h5>
                    </div>
                    <div class="col-md-4 text-left text-md-right text-dark">
                        Tanggal Pinjam : {{ date('d/m/Y', strtotime($transaction->tanggal_pinjam)) }}
                    </div>
                </div>
                <hr>

                {{-- info --}}
                <div class="row mb-4">
                    <div class="col-md-4 mb-3 text-dark">
                        <span class="d-block">Diinput Oleh.</span>
                        <span class="d-block font-weight-bold">{{ $transaction->user->nama }}</span>
                    </div>
                    <div class="col-md-4 mb-3 text-dark">
                        <span class="d-block">Peminjam</span>
                        <span class="d-block font-weight-bold">{{ $transaction->student->nama }} [{{ $transaction->student->nis }}]</span>
                        <span class="d-block font-weight-bold">{{ $transaction->student->alamat }}</span>
                    </div>
                    <div class="col-md-4 mb-3 text-dark">
                        <span class="d-block font-weight-bold">#{{ $transaction->kd_pinjam }}</span>
                        <span class="d-block font-weight-bold">Jatuh Tempo : {{ date('d/m/Y', strtotime($transaction->tanggal_tempo)) }}</span>
                    </div>
                </div>
                {{-- end info --}}

                {{-- table --}}
                <div class="scroll-x">
                    <table class="table table-striped table-bordered text-center w-100">
                        <thead class="bg-primary text-white">
                            <th>ID Buku</th>
                            <th>Judul</th>
                            <th>Pengarang</th>
                            <th>ISBN</th>
                            <th>Qty</th>
                        </thead>
                        <tbody>
                            @php
                                $total = 0;
                            @endphp
                            @foreach ($transaction->books as $book)
                                <tr>
                                    <td>{{ $book->kd_buku }}</td>
                                    <td>{{ $book->judul }}</td>
                                    <td>{{ $book->pengarang }}</td>
                                    <td>{{ $book->isbn }}</td>
                                    <td>{{ $book->pivot->qty }}</td>
                                </tr>
                                @php
                                    $total += $book->pivot->qty;
                                @endphp
                            @endforeach
                        </tbody>
                        <tfoot class="text-dark">
                            <tr>
                                <td class="text-left" colspan="4">Total Buku</td>
                                <td>{{ $total }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{-- end table --}}

                {{-- keterangan --}}
                <div class="my-4">
                    <span class="d-block font-weight-bold text-dark">Keterangan: </span>
                    <p class="text-dark">{{ $transaction->keterangan }}</p>
                </div>
                {{-- end keterangan --}}

                <a href="/peminjaman" class="btn btn-dark btn-sm rounded-0">
                    <i class="fas fa-arrow-left"></i> Kembali
                </a>

            </div>
        </div>
        {{-- end card --}}
        
    </div>
</div>

@endsection