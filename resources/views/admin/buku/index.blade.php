@extends('layouts.admin')
@section('title', 'Data Buku')
@section('master', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/buku/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah Buku
                </a>

                {{-- table --}}
                <div class="table-responsive mt-4">
                    <table class="table table-striped table-bordered text-center w-100" id="table-buku">
                        <thead>
                            <th>ID</th>
                            <th>Judul</th>
                            <th>Pengarang</th>
                            <th>Jumlah</th>
                            <th>Buku Tersisa</th>
                            <th>Kategori</th>
                            <th>Penerbit</th>
                            <th style="min-width: 170px">Opsi</th>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    <script>
        var table = $('#table-buku').DataTable({
            processing: true,
            order: [],
            language: {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/buku/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "id"},
                {data: "judul"},
                {data: "pengarang"},
                {data: "jumlah"},
                {data: "tersisa"},
                {data: "kategori"},
                {data: "penerbit"},
                {data: "opsi", searchable: false, orderable: false},
            ]
        });
    </script>
    <script src="/js/delete.js"></script>
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection