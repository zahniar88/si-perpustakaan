@extends('layouts.admin')
@section('title', 'Ubah Buku')
@section('master', 'active-link')

@section('content')
    
<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/buku/{{ $book->id }}" method="post" class="scroll-x py-4">
                    @csrf @method('PUT')

                    {{-- kategori --}}
                    <div class="form-group row">
                        <label for="kategori" class="col-sm-2 col-form-label">Kategori</label>
                        <div class="col-sm-10">
                            <select name="kategori" id="kategori" class="custom-select w-100 rounded-0 select-option @error('kategori') is-invalid @enderror">
                                <option>Pilih Kategori</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ (old('kategori', $book->category_id) == $category->id) ? 'selected' : '' }}>{{ $category->kd_kategori }} - {{ $category->nama }}</option>
                                @endforeach
                            </select>
                            @error('kategori')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- judul --}}
                    <div class="form-group row">
                        <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('judul') is-invalid @enderror" name="judul" id="judul" placeholder="Judul Buku" autocomplete="off" value="{{ old('judul', $book->judul) }}">
                            @error('judul')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- isbn --}}
                    <div class="form-group row">
                        <label for="isbn" class="col-sm-2 col-form-label">ISBN</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('isbn') is-invalid @enderror" name="isbn" id="isbn" placeholder="ISBN" autocomplete="off" value="{{ old('isbn', $book->isbn) }}">
                            @error('isbn')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- pengarang --}}
                    <div class="form-group row">
                        <label for="pengarang" class="col-sm-2 col-form-label">Pengarang</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('pengarang') is-invalid @enderror" name="pengarang" id="pengarang" placeholder="Nama Pengarang" autocomplete="off" value="{{ old('pengarang', $book->pengarang) }}">
                            @error('pengarang')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- halaman --}}
                    <div class="form-group row">
                        <label for="halaman" class="col-sm-2 col-form-label">Halaman</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('halaman') is-invalid @enderror" name="halaman" id="halaman" placeholder="Jumlah Halaman" autocomplete="off" value="{{ old('halaman', $book->halaman) }}">
                            @error('halaman')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- jumlah --}}
                    <div class="form-group row">
                        <label for="jumlah" class="col-sm-2 col-form-label">Jumlah</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('jumlah') is-invalid @enderror" name="jumlah" id="jumlah" placeholder="Jumlah Buku" autocomplete="off" value="{{ old('jumlah', $book->jumlah) }}">
                            @error('jumlah')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- tahun --}}
                    <div class="form-group row">
                        <label for="tahun" class="col-sm-2 col-form-label">Tahun Terbit</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('tahun') is-invalid @enderror" name="tahun" id="tahun" placeholder="Tahun Terbit" autocomplete="off" value="{{ old('tahun', $book->thn_terbit) }}">
                            @error('tahun')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- sinopsis --}}
                    <div class="form-group row">
                        <label for="sinopsis" class="col-sm-2 col-form-label">Sinopsis</label>
                        <div class="col-sm-10">
                            <textarea name="sinopsis" id="sinopsis" class="form-control rounded-0 @error('sinopsis') is-invalid @enderror" placeholder="Sinopsis">{{ old('sinopsis', $book->sinopsis) }}</textarea>
                            @error('sinopsis')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- penerbit --}}
                    <div class="form-group row">
                        <label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
                        <div class="col-sm-10">
                            <select name="penerbit" id="penerbit" class="custom-select rounded-0 select-option @error('penerbit') is-invalid @enderror">
                                <option>Pilih Penerbit</option>
                                @foreach ($publishers as $publisher)
                                    <option value="{{ $publisher->id }}" {{ (old('penerbit', $book->publisher_id) == $publisher->id) ? 'selected' : '' }}>{{ $publisher->kd_penerbit }} - {{ $publisher->nama }}</option>
                                @endforeach
                            </select>
                            @error('penerbit')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- rak --}}
                    <div class="form-group row">
                        <label for="rak" class="col-sm-2 col-form-label">Rak</label>
                        <div class="col-sm-10">
                            <select name="rak" id="rak" class="custom-select rounded-0 select-option @error('rak') is-invalid @enderror">
                                <option>Pilih Rak</option>
                                @foreach ($racks as $rack)
                                    <option value="{{ $rack->id }}" {{ (old('rak', $book->rack_id) == $rack->id) ? 'selected' : '' }}>{{ $rack->nama }} [{{ $rack->keterangan }}]</option>
                                @endforeach
                            </select>
                            @error('rak')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <a href="/buku" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.select-option').select2();
        });
    </script>
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection