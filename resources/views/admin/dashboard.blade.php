@extends('layouts.admin')
@section('title', 'Dashboard')
@section('dashboard', 'active-link')

@section('content')

<div class="alert alert-info my-4">
    Harap rapihkan kembali buku ke tempatnya saat siswa mengembalikan buku yang telah dipinjam
</div>

<!-- Info -->
<div class="row">

    <!-- Siswa -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">SISWA</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $student }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Buku -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">BUKU</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $book }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-book fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pengadaan -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">PENGADAAN</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $procurement }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-shopping-cart fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Peminjaman -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">PEMINJAMAN</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $transaction }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-balance-scale fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end info --}}

{{-- info 2 --}}
<div class="row">
    <div class="col-md-8 mb-3">
        <div class="card shadow border-0 rounded-0 border-left-primary">
            <div class="card-header border-0">
                Peminjaman Buku Terakhir
            </div>
            <div class="card-body rounded-0">

                {{-- table --}}
                <div class="scroll-x">
                    <table class="table table-triped table-bordered text-center w-100">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tgl Pinjam</th>
                                <th>Siswa</th>
                                <th>Jatuh Tempo</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->kd_pinjam }}</td>
                                    <td>{{ $transaction->tanggal_pinjam }}</td>
                                    <td>{{ $transaction->student->nama }}</td>
                                    <td>{{ $transaction->tanggal_tempo }}</td>
                                    <td>
                                        <span class="badge badge-warning rounded-0 p-1 font-weight-normal">Pinjam</span>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">Tidak ada data pad table ini</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
    </div>
    <div class="col-md-4 mb-3">
        <div class="card shadow border-0 rounded-0 border-left-danger">
            <div class="card-header border-0">
                Buku Paling Populer
            </div>
            <ul class="list-group">
                @forelse ($books as $book)
                    <li class="list-group-item">
                        <p class="mb-0 text-primary">{{ $book->judul }}</p>
                        <small class="mb-0">{{ $book->pengarang }}</small>
                    </li>
                @empty
                    
                @endforelse
            </ul>
        </div>
    </div>
</div>
{{-- end info 2 --}}

@endsection