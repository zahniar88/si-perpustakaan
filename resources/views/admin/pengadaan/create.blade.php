@extends('layouts.admin')
@section('title', 'Tambah Pengadaan')
@section('pengadaan', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card rounded-0 border-0 shadow border-left-primary">
            <div class="card-body rounded-0">

                <div class="alert alert-info">
                    <i class="fas fa-info-circle"></i> 
                    Silahkan input data buku di menu <b>Data Master</b> <i class="fas fa-arrow-right"></i> <b>Buku</b> jika judul buku tidak terdapat dalam pilihan
                </div>

                {{-- form --}}
                <form action="/pengadaan" method="post">
                    @csrf

                    {{-- buku --}}
                    <div class="form-group row">
                        <label for="buku" class="col-sm-2 col-form-label">Buku</label>
                        <div class="col-sm-10">
                            <select name="buku" id="buku" class="custom-select rounded-0 @error('buku') is-invalid @enderror">
                                <option>Pilih Buku</option>
                                @foreach ($books as $book)
                                    <option value="{{ $book->id }}" {{ old('buku') == $book->id ? 'selected' : '' }}>{{ $book->judul }}</option>
                                @endforeach
                            </select>
                            @error('buku')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- asal --}}
                    <div class="form-group row">
                        <label for="asal" class="col-sm-2 col-form-label">asal</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('asal') is-invalid @enderror" name="asal" id="asal" placeholder="Asal Buku" autocomplete="off" value="{{ old('asal') }}">
                            @error('asal')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- jumlah --}}
                    <div class="form-group row">
                        <label for="jumlah" class="col-sm-2 col-form-label">Jumlah Masuk</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control rounded-0 @error('jumlah') is-invalid @enderror" name="jumlah" id="jumlah" placeholder="Jumlah Masuk" autocomplete="off" value="{{ old('jumlah') }}">
                            @error('jumlah')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    {{-- Keterangan --}}
                    <div class="form-group row">
                        <label for="Keterangan" class="col-sm-2 col-form-label">Keterangan Masuk</label>
                        <div class="col-sm-10">
                            <textarea name="keterangan" id="keterangan" class="form-control rounded-0 @error('keterangan') is-invalid @enderror" placeholder="Keterangan">{{ old('keterangan') }}</textarea>
                            @error('keterangan')
                                <small class="invalid-feedback">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <a href="/pengadaan" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    <script>
        $('#buku').select2();
    </script>
@endsection