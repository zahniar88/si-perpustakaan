@extends('layouts.admin')
@section('title', 'Data Pengadaan')
@section('pengadaan', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/pengadaan/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah Pengadaan
                </a>

                {{-- table --}}
                <div class="mt-4 table-responsive">
                    <table class="table table-striped table-bordered text-center w-100" width="100%" id="table-pengadaan" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Judul</th>
                                <th>Asal Buku</th>
                                <th>Jumlah</th>
                                <th>Keterangan</th>
                                <th style="min-width: 130px">Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
<script>
    var table = $('#table-pengadaan').DataTable({
            processing: true,
            order: [],
            language: {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/pengadaan/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "tanggal"},
                {data: "judul"},
                {data: "asal"},
                {data: "jumlah"},
                {data: "keterangan"},
                {data: "opsi", searchable: false, orderable: false},
            ]
        });
</script>
<script src="/js/delete.js"></script>
@if ( session('response') )
    {!! session('response') !!}
@endif
@endsection