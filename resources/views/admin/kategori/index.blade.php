@extends('layouts.admin')
@section('title', 'Data Kategori')
@section('master', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-left-primary">
            <div class="card-body rounded-0">

                <a href="/kategori/create" class="btn btn-primary btn-sm rounded-0">
                    <i class="fas fa-plus"></i> Tambah Kategori
                </a>

                {{-- table --}}
                <div class="table-responsive mt-4">
                    <table class="table table-bordered table-striped text-center w-100" id="table-kategori">
                        <thead>
                            <th>ID</th>
                            <th>Kategori</th>
                            <th style="max-width: 180px;">Opsi</th>
                        </thead>
                    </table>
                </div>
                {{-- end table --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    <script>
        var table = $('#table-kategori').DataTable({
            processing: true,
            order: [],
            language:  {
                url: "/vendor/datatables/indonesian.json"
            },
            ajax: {
                url: "/kategori/ajax",
                dataSrc: "",
            },
            columns : [
                {data: "id"},
                {data: "kategori"},
                {data: "opsi", searchable: false, orderable: false},
            ]
        });
    </script>
    <script src="/js/delete.js"></script>
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection
