@extends('layouts.admin')
@section('title', 'Ubah Kategori')
@section('master', 'active-link')

@section('content')
    
<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/kategori/{{ $category->id }}" method="post">
                    @csrf @method('PUT')

                    {{-- kategori --}}
                    <div class="form-group">
                        <label for="kategori">Kategori</label>
                        <input type="text" class="form-control rounded-0 @error('kategori') is-invalid @enderror" name="kategori" id="kategori" autocomplete="off" placeholder="Nama Kategori" value="{{ old('kategori', $category->nama) }}">
                        @error('kategori')
                            <small class="invalid-feedback">{{ $message }}</small>
                        @enderror
                    </div>

                    <a href="/kategori" class="btn btn-dark btn-sm rounded-0">
                        <i class="fas fa-times"></i> Batal
                    </a>
                    <button class="btn btn-success btn-sm rounded-0">
                        <i class="fas fa-check"></i> Submit
                    </button>

                </form>
                {{-- end form --}}

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    @if ( session('response') )
        {!! session('response') !!}
    @endif
@endsection