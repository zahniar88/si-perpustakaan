@extends('layouts.admin')
@section('title', 'Laporan Peminjaman')
@section('laporan', 'active-link')

@section('content')

<div class="row">
    <div class="col-md-12">

        {{-- card --}}
        <div class="card shadow rounded-0 border-0 border-left-primary">
            <div class="card-body rounded-0">

                {{-- form --}}
                <form action="/laporan-peminjaman" method="get" class="mb-5">
                    
                    {{-- from --}}
                    <div class="form-group row">
                        <label for="tanggal_awal" class="col-sm-2 col-form-label">Dari Tanggal</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control rounded-0 date" name="tanggal_awal" id="tanggal_awal" autocomplete="off" value="{{ Request::input('tanggal_awal') }}" placeholder="dd-mm-yyyy">
                        </div>
                    </div>

                    {{-- to --}}
                    <div class="form-group row">
                        <label for="tanggal_akhir" class="col-sm-2 col-form-label">Sampai Tanggal</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control rounded-0 date" name="tanggal_akhir" id="tanggal_akhir" autocomplete="off" value="{{ Request::input('tanggal_akhir') }}" placeholder="dd-mm-yyyy">
                        </div>
                    </div>

                    <button class="btn btn-primary btn-sm rounded-0">
                        <i class="fas fa-check"></i> Tampilkan
                    </button>

                </form>
                {{-- end form --}}

                {{-- table --}}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-center w-100">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tanggal Pinjam</th>
                                <th>Jatuh Tempo</th>
                                <th>Nama Siswa</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->kd_pinjam }}</td>
                                    <td>{{ $transaction->tanggal_pinjam }}</td>
                                    <td>{{ $transaction->tanggal_tempo }}</td>
                                    <td>{{ $transaction->student->nama }}</td>
                                    <td>{{ $transaction->status == 0 ? 'Kembali' : 'Pinjam' }}</td>
                                    <td>{{ $transaction->keterangan }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Tidak ada data tersedia</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $transactions->appends(Request::input())->onEachSide(1)->links() }}
                </div>
                {{-- end table --}}
                @if ( Request::input() )
                    <a href="/laporan-peminjaman/print?tanggal_awal={{ Request::input('tanggal_awal') }}&tanggal_akhir={{ Request::input('tanggal_akhir') }}" class="btn btn-dark btn-sm rounded-0 px-3" target="_blank">
                        <i class="fas fa-print"></i> Print
                    </a>
                @endif

            </div>
        </div>
        {{-- end card --}}

    </div>
</div>

@endsection

@section('js')
    <script>
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    </script>
@endsection