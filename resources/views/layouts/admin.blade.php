<!DOCTYPE html>
<html lang="id">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name'))</title>

    <!-- Custom fonts for this template-->
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/bootstrap-datepicker3.standalone.min.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendor/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/css/style.css">
    @yield('css')

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-super-dark sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/" target="_blank">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-book"></i>
                </div>
                <div class="sidebar-brand-text mx-3">SI PERPUS</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Navigasi
            </div>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item @yield('dashboard')">
                <a class="nav-link" href="/dashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            @if ( Auth::user()->admin() || Auth::user()->petugas() )
                <!-- Nav Item - Pengadaan Buku -->
                <li class="nav-item @yield('pengadaan')">
                    <a class="nav-link" href="/pengadaan">
                        <i class="fas fa-fw fa-book"></i>
                        <span>Pengadaan Buku</span>
                    </a>
                </li>
            @endif

            @if ( Auth::user()->admin() )
                <!-- Nav Item - Data Pengguna -->
                <li class="nav-item @yield('pengguna')">
                    <a class="nav-link" href="/pengguna">
                        <i class="fas fa-fw fa-user"></i>
                        <span>Data Pengguna</span>
                    </a>
                </li>
            @endif

            @if ( Auth::user()->admin() || Auth::user()->petugas() )
                <!-- Nav Item - Data Master -->
                <li class="nav-item @yield('master')">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDataMaster" aria-expanded="true"
                        aria-controls="collapseDataMaster">
                        <i class="fas fa-fw fa-edit"></i>
                        <span>Data Master</span>
                    </a>
                    <div id="collapseDataMaster" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded-0">
                            <h6 class="collapse-header">Data Master:</h6>
                            <a class="collapse-item" href="/siswa">
                                <i class="fas fa-dot-circle mr-2"></i> Siswa
                            </a>
                            <a class="collapse-item" href="/buku">
                                <i class="fas fa-dot-circle mr-2"></i> Buku
                            </a>
                            <a class="collapse-item" href="/kategori">
                                <i class="fas fa-dot-circle mr-2"></i> Kategori
                            </a>
                            <a class="collapse-item" href="/penerbit">
                                <i class="fas fa-dot-circle mr-2"></i> Penerbit
                            </a>
                            <a class="collapse-item" href="/rak">
                                <i class="fas fa-dot-circle mr-2"></i> Rak
                            </a>
                        </div>
                    </div>
                </li>
                
                <!-- Nav Item - Data Transaksi -->
                <li class="nav-item @yield('transaksi')">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDataTransaksi"
                        aria-expanded="true" aria-controls="collapseDataTransaksi">
                        <i class="fas fa-fw fa-balance-scale"></i>
                        <span>Data Transaksi</span>
                    </a>
                    <div id="collapseDataTransaksi" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded-0">
                            <h6 class="collapse-header">Data Transaksi:</h6>
                            <a class="collapse-item" href="/peminjaman">
                                <i class="fas fa-dot-circle mr-2"></i> Peminjaman Buku
                            </a>
                            <a class="collapse-item" href="/pengembalian">
                                <i class="fas fa-dot-circle mr-2"></i> Pengembalian Buku
                            </a>
                        </div>
                    </div>
                </li>
            @endif

            @if ( Auth::user()->admin() || Auth::user()->kepala() )
                <!-- Nav Item - Laporan -->
                <li class="nav-item @yield('laporan')">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLaporan" aria-expanded="true"
                        aria-controls="collapseLaporan">
                        <i class="fas fa-fw fa-chart-pie"></i>
                        <span>Laporan</span>
                    </a>
                    <div id="collapseLaporan" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded-0">
                            <h6 class="collapse-header">Laporan:</h6>
                            <a class="collapse-item" href="/laporan-pengadaan">
                                <i class="fas fa-dot-circle mr-2"></i> Pengadaan Buku
                            </a>
                            <a class="collapse-item" href="/laporan-peminjaman">
                                <i class="fas fa-dot-circle mr-2"></i> Peminjaman Buku
                            </a>
                        </div>
                    </div>
                </li>
            @endif

            @if ( Auth::user()->admin() )
                <!-- Nav Item - Pengaturan Umum -->
                <li class="nav-item @yield('pengaturan')">
                    <a class="nav-link" href="/pengaturan">
                        <i class="fas fa-fw fa-cogs"></i>
                        <span>Pengaturan Umum</span>
                    </a>
                </li>
            @endif

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block my-3">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-dark bg-primary topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-primary d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars text-white"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-white">{{ Auth::user()->nama }}</span>
                                <img class="img-profile rounded-circle" src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}?d=mp">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow rounded-0 animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="/profil">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/logout">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <h1 class="h3 mb-4 text-gray-800">@yield('title', config('app.name'))</h1>

                    @yield('content')

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>
                            Copyright &copy; 2020 By <a href="http://instagram.com/zahniar.space">Zahniar Adirahman</a>
                        </span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="/js/app.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/js/sb-admin-2.min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/select2.min.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    @yield('js')

</body>

</html>