<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    {{-- css --}}
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>

    @yield('content')
    
    {{-- js --}}
    <script src="/js/app.js"></script>
    @yield('js')
</body>
</html>