<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    {{-- css --}}
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

    {{-- navbar --}}
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark py-3">
        <div class="container">
            <a class="navbar-brand" href="/" style="font-size: 23px">PERPUSTAKAAN</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMainMenu"
                aria-controls="navbarMainMenu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarMainMenu">
                <div class="navbar-nav ml-auto">
                    <a class="nav-item nav-link text-white" href="/login">
                        <i class="fas fa-user"></i> PERPUSTAKAAN
                    </a>
                </div>
            </div>
        </div>
    </nav>
    {{-- end navbar --}}

    {{-- jumbotron --}}
    <div class="jumbotron rounded-0 bg-perpus mb-0">
        <div class="container text-center py-5">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="display-4 mb-0 text-white">Budayakan Membaca Buku</h1>
                </div>
            </div>
        </div>
    </div>
    {{-- end jumbotron --}}

    {{-- content --}}
    <div class="container py-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card rounded-0 border-0 shadow">
                    <div class="card-body rounded-0">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end content --}}

    {{-- footer --}}
    <footer class="bg-dark py-4">
        <div class="container text-center text-secondary">
            Copyright &copy; 2020 By Zahniar Adirahman
        </div>
    </footer>
    {{-- end footer --}}
    
    {{-- js --}}
    <script src="/js/app.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="/js/landing.js"></script>

</body>
</html>