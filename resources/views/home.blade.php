@extends('layouts.app')
@section('title', config('app.name'))

@section('content')

{{-- form --}}
<form action="" class="input-group" method="get" id="filter">
    <input type="text" class="form-control" name="search" id="search" placeholder="Judul Buku" autocomplete="off">
    <div class="input-group-append">
        <button class="btn btn-primary">
            <i class="fas fa-search"></i>
        </button>
    </div>
</form>
{{-- end form --}}

<div class="row mt-4" id="content"></div>
<hr class="my-3">
<div id="pagination"></div>

@endsection