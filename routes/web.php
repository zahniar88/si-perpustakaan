<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'HomeController@index')->middleware('guest');
Route::post('/data-landing', 'HomeController@index_ajax')->middleware('guest');

// route for authenticate
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

// route for admin
Route::middleware(['auth', 'activate'])->group(function() {

    // dashboard
    Route::get('/admin', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index');

    // Category
    Route::get('/kategori/ajax', 'CategoryController@index_ajax');
    Route::resource('/kategori', 'CategoryController')->except('show');

    // Publisher
    Route::get('/penerbit/ajax', 'PublisherController@index_ajax');
    Route::resource('/penerbit', 'PublisherController')->except('show');

    // Rack
    Route::get('/rak/ajax', 'RackController@index_ajax');
    Route::resource('/rak', 'RackController')->except('show');

    // Book
    Route::get('/buku/ajax', 'BookController@index_ajax');
    Route::resource('/buku', 'BookController')->except('show');

    // Student
    Route::get('/siswa/ajax', 'StudentController@index_ajax');
    Route::resource('/siswa', 'StudentController')->except('show');

    // Admin
    Route::get('/pengguna/ajax', 'AdminController@index_ajax');
    Route::resource('/pengguna', 'AdminController')->except('show');

    // Procurement
    Route::get('/pengadaan/ajax', 'ProcurementController@index_ajax');
    Route::resource('/pengadaan', 'ProcurementController')->except(['show', 'update', 'edit']);

    // Borrowing
    Route::get('/peminjaman/ajax', 'BorrowController@index_ajax');
    Route::post('/peminjaman/buku', 'BorrowController@find_book');
    Route::resource('/peminjaman', 'BorrowController')->except(['edit', 'update', 'destroy']);

    // Returning
    Route::get('/pengembalian/ajax', 'ReturnController@index_ajax');
    Route::get('/pengembalian/denda', 'ReturnController@get_denda');
    Route::get('/pengembalian/buku', 'ReturnController@get_buku');
    Route::resource('/pengembalian', 'ReturnController')->except(['edit', 'update', 'show', 'destroy']);

    // Report Procurement
    Route::get('/laporan-pengadaan', 'ReportController@index_procurement');
    Route::get('/laporan-pengadaan/print', 'ReportController@procurement_print');

    // Report Transaction
    Route::get('/laporan-peminjaman', 'ReportController@index_transaction');
    Route::get('/laporan-peminjaman/print', 'ReportController@transaction_print');

    // profil
    Route::get('/profil', 'ProfilController@edit');
    Route::put('/profil', 'ProfilController@update');

    // Setting
    Route::get('/pengaturan', 'SettingController@index');
    Route::put('/pengaturan/{id}', 'SettingController@update');

});