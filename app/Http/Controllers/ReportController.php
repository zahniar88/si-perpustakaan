<?php

namespace App\Http\Controllers;

use App\Models\Procurement;
use App\Models\Transaction;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    public function __construct() 
    {
        $this->middleware('AdminKepala');
    }
    
    /**
     * Index report recurement
     */
    public function index_procurement(Request $request)
    {
        if ( empty($request->tanggal_awal) || empty($request->tanggal_akhir) ) {
            $procurements = Procurement::with('book')->orderBy('id', 'DESC')->paginate(10);
        } else {
            $procurements = Procurement::with('book')->whereBetween('tanggal', [$request->tanggal_awal, $request->tanggal_akhir])->orderBy('id', 'DESC')->paginate(10);
        }

        return view('admin.laporan.index-pengadaan', compact('procurements'));
    }

    /**
     * Index report transaction
     */
    public function index_transaction(Request $request)
    {
        if (empty($request->tanggal_awal) || empty($request->tanggal_akhir)) {
            $transactions = Transaction::with('student')->orderBy('id', 'DESC')->paginate(10);
        } else {
            $transactions = Transaction::with('student')->whereBetween('tanggal_pinjam', [$request->tanggal_awal, $request->tanggal_akhir])->orderBy('id', 'DESC')->paginate(10);
        }

        return view('admin.laporan.index-peminjaman', compact('transactions'));
    }

    /**
     * Print Report Procurement
     */
    public function procurement_print(Request $request)
    {
        $procurements = Procurement::with('book')->whereBetween('tanggal', [$request->tanggal_awal, $request->tanggal_akhir])->orderBy('id', 'DESC')->get();

        return view('admin.print.procurement', compact('procurements'));
    }

    /**
     * Print Report Transaction
     */
    public function transaction_print(Request $request)
    {
        $transactions = Transaction::with('student')->whereBetween('tanggal_pinjam', [$request->tanggal_awal, $request->tanggal_akhir])->orderBy('id', 'DESC')->get();

        return view('admin.print.transaction', compact('transactions'));
    }
}