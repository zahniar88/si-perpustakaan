<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class SettingController extends Controller
{
    
    public function __construct() 
    {
        $this->middleware('Admin');
    }

    /**
     * Display the index setting
     */
    public function index()
    {
        $setting = Setting::first();
        return view('admin.setting.index', compact('setting'));
    }

    /**
     * Update data setting
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'denda' => ['required', 'numeric', 'digits_between:1,11'],
            'lama_pinjam' => ['required', 'numeric', 'digits_between:1,3']
        ]);

        $setting = Setting::find($id)->update([
            'denda' => $request->denda,
            'lama_pinjam' => $request->lama_pinjam
        ]);

        if ( $setting ) {
            return back()->with('response', Notify::success('Pengaturan telah disimpan'));
        } else {
            return back()->with('response', Notify::error('Pengaturan gagal disimpan'))->withInput();
        }
        
    }
}
