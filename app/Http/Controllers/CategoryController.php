<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class CategoryController extends Controller
{

    public function __construct() {
        $this->middleware('AdminPetugas');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.kategori.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $categories = Category::orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($categories as $category) {
            $data[] = [
                'id' => $category->kd_kategori,
                'kategori' => $category->nama,
                'opsi' => '
                    <form action="/kategori/' . $category->id . '" method="post">
                        ' . csrf_field() . method_field('DELETE') . ' 
                        <a href="/kategori/' . $category->kd_kategori . '/edit" class="btn btn-primary btn-sm rounded-0">
                            <i class="fas fa-edit"></i> Ubah
                        </a>
                        <button class="btn btn-danger btn-sm rounded-0" btn-delete>
                            <i class="fas fa-trash-alt"></i> Hapus
                        </button>
                    </form>
                '
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'kategori' => ['required', 'max:40', 'string', 'unique:categories,nama']
        ]);
        $ctg = new Category();

        $category = Category::create([
            'kd_kategori' => $ctg->getCode(),
            'nama' => $request->kategori
        ]);

        if ( $category ) {
            return redirect('/kategori')->with('response', Notify::success('Data kategori berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data kategori gagal ditambahkan'))->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kd_kategori)
    {
        $category = Category::where('kd_kategori', $kd_kategori)->firstOrFail();
        return view('admin.kategori.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi
        $request->validate([
            'kategori' => ['required', 'max:40', 'string', 'unique:categories,nama,'.$id]
        ]);

        $category = Category::findOrFail($id)->update([
            'nama' => $request->kategori
        ]);

        if ($category) {
            return redirect('/kategori')->with('response', Notify::success('Data kategori berhasil diubah'));
        } else {
            return back()->with('response', Notify::error('Data kategori gagal diubah'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::where('id', $id)->doesntHave('books')->first();
        
        if ( $category ) {
            if ($category->delete()) {
                return back()->with('response', Notify::success('Data berhasil dihapus'));
            } else {
                return back()->with('response', Notify::success('Data gagal dihapus'));
            }
        } else {
            return back()->with('response', Notify::error('Gagal, kategori ini telah digunakan di data buku'));
        }
        
    }
}
