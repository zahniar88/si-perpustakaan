<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use App\Models\Publisher;
use App\Models\Rack;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class BookController extends Controller
{

    public function __construct() {
        $this->middleware('AdminPetugas');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.buku.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $books = Book::with(['publisher', 'category', 'rack', 'transactions'])->orderBy('id', 'DESC')->get();
        
        $data = [];
        foreach ($books as $book) {
            
            // qty
            $sisa = $book->jumlah;
            foreach ($book->transactions()->where('status', 1)->get() as $transaction) {
                $sisa = $book->jumlah - $transaction->pivot->qty;
            }

            $data[] = [
                'id' => strip_tags($book->kd_buku),
                'judul' => strip_tags($book->judul),
                'pengarang' => strip_tags($book->pengarang),
                'jumlah' => strip_tags($book->jumlah),
                'tersisa' => $sisa,
                'kategori' => $book->category->nama,
                'penerbit' => $book->publisher->nama,
                'opsi' => '
                    <form action="/buku/' . $book->id . '" method="post">
                        ' . csrf_field() . method_field('DELETE') . '
                        <a href="/buku/' . $book->kd_buku . '/edit" class="btn btn-primary btn-sm rounded-0">
                            <i class="fas fa-edit"></i> Ubah
                        </a>
                        <button class="btn btn-danger btn-sm rounded-0" btn-delete>
                            <i class="fas fa-trash-alt"></i> Hapus
                        </button>
                    </form>
                ',
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $publishers = Publisher::all();
        $racks      = Rack::all();
        return view('admin.buku.create', compact('categories', 'publishers', 'racks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kategori' => ['required', 'exists:categories,id'],
            'judul' => ['required', 'max:255', 'string'],
            'isbn' => ['required', 'max:255', 'string'],
            'pengarang' => ['required', 'max:40', 'string'],
            'halaman' => ['required', 'numeric', 'digits_between:1,11'],
            'jumlah' => ['required', 'numeric', 'digits_between:1,11'],
            'tahun' => ['required', 'max:5', 'date_format:Y'],
            'sinopsis' => ['required', 'max:255', 'string'],
            'penerbit' => ['required', 'exists:publishers,id'],
            'rak' => ['required', 'exists:racks,id']
        ]);

        $bk = new Book();

        $book = Book::create([
            'kd_buku' => $bk->getCode(),
            'judul' => $request->judul,
            'isbn' => $request->isbn,
            'pengarang' => $request->pengarang,
            'halaman' => $request->halaman,
            'jumlah' => $request->jumlah,
            'thn_terbit' => $request->tahun,
            'sinopsis' => $request->sinopsis,
            'category_id' => $request->kategori,
            'publisher_id' => $request->penerbit,
            'rack_id' => $request->rak
        ]);

        if ( $book ) {
            return redirect('/buku')->with('response', Notify::success('Data buku berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data buku gagal ditambahkan'))->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kd_buku)
    {
        $book = Book::where('kd_buku', $kd_buku)->firstOrFail();
        $categories = Category::all();
        $publishers = Publisher::all();
        $racks      = Rack::all();
        return view('admin.buku.edit', compact('categories', 'publishers', 'racks', 'book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kategori' => ['required', 'exists:categories,id'],
            'judul' => ['required', 'max:255', 'string'],
            'isbn' => ['required', 'max:255', 'string'],
            'pengarang' => ['required', 'max:40', 'string'],
            'halaman' => ['required', 'numeric', 'digits_between:1,11'],
            'jumlah' => ['required', 'numeric', 'digits_between:1,11'],
            'tahun' => ['required', 'max:5', 'date_format:Y'],
            'sinopsis' => ['required', 'max:255', 'string'],
            'penerbit' => ['required', 'exists:publishers,id'],
            'rak' => ['required', 'exists:racks,id']
        ]);

        $book = Book::findOrFail($id)->update([
            'judul' => $request->judul,
            'isbn' => $request->isbn,
            'pengarang' => $request->pengarang,
            'halaman' => $request->halaman,
            'jumlah' => $request->jumlah,
            'thn_terbit' => $request->tahun,
            'sinopsis' => $request->sinopsis,
            'category_id' => $request->kategori,
            'publisher_id' => $request->penerbit,
            'rack_id' => $request->rak
        ]);

        if ($book) {
            return redirect('/buku')->with('response', Notify::success('Data buku berhasil diubah'));
        } else {
            return back()->with('response', Notify::error('Data buku gagal diubah'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::where('id', $id)->doesntHave('transactions')->first();
        
        if ( $book ) {
            if ($book->delete()) {
                return back()->with('response', Notify::success('Data berhasil dihapus'));
            } else {
                return back()->with('response', Notify::error('Data gagal dihapus'));
            }
        } else {
            return back()->with('response', Notify::error('Gagal, buku telah dipinjam oleh siswa'));
        }
        
    }
}
