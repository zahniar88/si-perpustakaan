<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class StudentController extends Controller
{

    public function __construct()
    {
        $this->middleware('AdminPetugas');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.siswa.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $students = Student::orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($students as $student) {
            $data[] = [
                'nis' => strip_tags($student->nis),
                'nama' => strip_tags($student->nama),
                'jk' => $student->jk,
                'alamat' => strip_tags($student->alamat),
                'opsi' => '
                    <form action="/siswa/' . $student->id . '" method="post">
                        ' . csrf_field() . method_field('DELETE') . '
                        <a href="/siswa/' . $student->nis . '/edit" class="btn btn-primary btn-sm rounded-0">
                            <i class="fas fa-edit"></i> Ubah
                        </a>
                        <button class="btn btn-danger btn-sm rounded-0" btn-delete>
                            <i class="fas fa-trash-alt"></i> Hapus
                        </button>
                    </form>
                ',
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'nis' => ['required', 'unique:students,nis'],
            'nama' => ['required', 'max:30', 'string'],
            'jenis_kelamin' => ['required', 'in:L,P'],
            'alamat' => ['required', 'max:255', 'string']
        ]);

        $student = Student::create([
            'nis' => $request->nis,
            'nama' => $request->nama,
            'jk' => $request->jenis_kelamin,
            'alamat' => $request->alamat
        ]);

        if ( $student ) {
            return redirect('/siswa')->with('response', Notify::success('Data siswa berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data siswa gagal ditambahkan'))->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nis)
    {
        $student = Student::where('nis', $nis)->firstOrFail();
        return view('admin.siswa.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi
        $request->validate([
            'nis' => ['required', 'unique:students,nis,'.$id],
            'nama' => ['required', 'max:30', 'string'],
            'jenis_kelamin' => ['required', 'in:L,P'],
            'alamat' => ['required', 'max:255', 'string']
        ]);

        $student = Student::findOrFail($id)->update([
            'nis' => $request->nis,
            'nama' => $request->nama,
            'jk' => $request->jenis_kelamin,
            'alamat' => $request->alamat
        ]);

        if ($student) {
            return redirect('/siswa')->with('response', Notify::success('Data siswa berhasil diubah'));
        } else {
            return back()->with('response', Notify::error('Data siswa gagal diubah'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::where('id', $id)->doesntHave('transactions')->first();
        if ( $student ) {
            if ($student->delete()) {
                return back()->with('response', Notify::success('Data berhasil dihapus'));
            } else {
                return back()->with('response', Notify::error('Data gagal dihapus'));
            }
        } else {
            return back()->with('response', Notify::error('Gagal, siswa ini telah melakukan peminjaman buku'));
        }
        
    }
}
