<?php

namespace App\Http\Controllers;

use App\Models\Rack;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class RackController extends Controller
{

    public function __construct()
    {
        $this->middleware('AdminPetugas');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.rak.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $racks = Rack::orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($racks as $rack) {
            $data[] = [
                'rak' => $rack->nama,
                'keterangan' => $rack->keterangan,
                'opsi' => '
                    <form action="/rak/' . $rack->id . '" method="post">
                        ' . csrf_field() . method_field('DELETE') . '
                        <a href="/rak/' . $rack->id . '/edit" class="btn btn-primary btn-sm rounded-0">
                            <i class="fas fa-edit"></i> Ubah
                        </a>
                        <button class="btn btn-danger btn-sm rounded-0" btn-delete>
                            <i class="fas fa-trash-alt"></i> Hapus
                        </button>
                    </form>
                ',
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rak.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'rak' => ['required', 'string', 'max:30', 'unique:racks,nama'],
            'keterangan' => ['required', 'string', 'max:255']
        ]);

        $rack = Rack::create([
            'nama' => $request->rak,
            'keterangan' => $request->keterangan
        ]);

        if ( $rack ) {
            return redirect('/rak')->with('response', Notify::success('Data rak berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data rak gagal ditambahkan'))->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rack = Rack::findOrFail($id);
        return view('admin.rak.edit', compact('rack'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /// validasi
        $request->validate([
            'rak' => ['required', 'string', 'max:30', 'unique:racks,nama,'.$id],
            'keterangan' => ['required', 'string', 'max:255']
        ]);

        $rack = Rack::findOrFail($id)->update([
            'nama' => $request->rak,
            'keterangan' => $request->keterangan
        ]);

        if ($rack) {
            return redirect('/rak')->with('response', Notify::success('Data rak berhasil diubah'));
        } else {
            return back()->with('response', Notify::error('Data rak gagal diubah'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rack = Rack::where('id', $id)->doesntHave('books')->first();
        
        if ( $rack ) {
            if ($rack->delete()) {
                return back()->with('response', Notify::success('Data berhasil dihapus'));
            } else {
                return back()->with('response', Notify::error('Data gagal dihapus'));
            }
        } else {
            return back()->with('response', Notify::error('Gagal, rak sudah digunakan di data buku'));
        }
        
        
    }
}
