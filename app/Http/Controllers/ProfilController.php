<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class ProfilController extends Controller
{
    /**
     * Show form edit profil
     */
    public function edit()
    {
        return view('admin.profil.edit');
    }

    /**
     * Run Update the profil
     */
    public function update(Request $request)
    {
        $request->validate([
            'nama' => ['required', 'max:30', 'string'],
            'email' => ['required', 'max:255', 'email', 'unique:users,email,'.auth()->user()->id],
            'username' => ['required', 'max:40', 'alpha_num', 'unique:users,username,' . auth()->user()->id],
            'password' => ['nullable', 'min:8', 'confirmed']
        ]);
        
        if ( $request->password == null ) {
            auth()->user()->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'username' => $request->username,
            ]);
            return back()->with('response', Notify::success('Data profil berhasil diperbarui'));
        } else {
            auth()->user()->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'username' => $request->username,
                'password' => bcrypt($request->password)
            ]);
            return back()->with('response', Notify::success('Data profil berhasil diperbarui'));
        }
        
    }
}
