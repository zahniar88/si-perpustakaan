<?php

namespace App\Http\Controllers;

use App\Models\Publisher;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class PublisherController extends Controller
{

    public function __construct() {
        $this->middleware('AdminPetugas');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.penerbit.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $publishers = Publisher::orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($publishers as $publisher) {
            $data[] = [
                'id' => $publisher->kd_penerbit,
                'penerbit' => $publisher->nama,
                'opsi' => '
                    <form action="/penerbit/' . $publisher->id . '" method="post">
                        ' . csrf_field() . method_field('DELETE') . ' 
                        <a href="/penerbit/' . $publisher->kd_penerbit . '/edit" class="btn btn-primary btn-sm rounded-0">
                            <i class="fas fa-edit"></i> Ubah
                        </a>
                        <button class="btn btn-danger btn-sm rounded-0" btn-delete>
                            <i class="fas fa-trash-alt"></i> Hapus
                        </button>
                    </form>
                '
            ];
        }

        return$data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.penerbit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'penerbit' => ['required', 'max:100', 'unique:publishers,nama']
        ]);

        $pbl = new Publisher();

        $publisher = Publisher::create([
            'kd_penerbit' => $pbl->getCode(),
            'nama' => $request->penerbit
        ]);

        if ( $publisher ) {
            return redirect('/penerbit')->with('response', Notify::success('Data penerbit berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::success('Data penerbit gagal ditambahkan'))->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kd_penerbit)
    {
        $publisher = Publisher::where('kd_penerbit', $kd_penerbit)->firstOrFail();
        return view('admin.penerbit.edit', compact('publisher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi
        $request->validate([
            'penerbit' => ['required', 'max:40', 'string', 'unique:publishers,nama,' . $id]
        ]);

        $category = Publisher::findOrFail($id)->update([
            'nama' => $request->penerbit
        ]);

        if ($category) {
            return redirect('/penerbit')->with('response', Notify::success('Data penerbit berhasil diubah'));
        } else {
            return back()->with('response', Notify::error('Data penerbit gagal diubah'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $publisher = Publisher::where('id', $id)->doesntHave('books')->first();
        
        if ( $publisher ) {
            if ($publisher->delete()) {
                return back()->with('response', Notify::success('Data berhasil dihapus'));
            } else {
                return back()->with('response', Notify::error('Data gagal dihapus'));
            }
        } else {
            return back()->with('response', Notify::error('Gagal, penerbit sudah digunakan di data buku'));
        }
        
    }
}
