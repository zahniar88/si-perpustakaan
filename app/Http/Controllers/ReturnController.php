<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Student;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class ReturnController extends Controller
{

    public function __construct() 
    {
        $this->middleware('AdminPetugas');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pengembalian.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $setting = Setting::first();
        $transactions = Transaction::with('student')->where('status', 0)->orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($transactions as $transaction) {
            $data[] = [
                'tgl_kembali' => $transaction->tanggal_kembali,
                'id' => $transaction->kd_pinjam,
                'siswa' => $transaction->student->nama,
                'tgl_pinjam' => $transaction->tanggal_pinjam,
                'tgl_tempo' => $transaction->tanggal_tempo,
                'denda' => $transaction->terlambat * $setting->denda,
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transactions = Transaction::with('student')->where('status', 1)->get();
        return view('admin.pengembalian.create', compact('transactions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'id' => ['required', 'exists:transactions,id']
        ]);

        $transaction = Transaction::find($request->id)->update([
            'tanggal_kembali' => now()->isoFormat('DD-MM-YYYY'),
            'terlambat' => $request->terlambat,
            'status' => 0
        ]);

        if ( $transaction ) {
            return redirect('/pengembalian')->with('response', Notify::success('Data pengembalian berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data pengembalian gagal ditambahkan'));
        }
        
        
    }

    /**
     * get denda
     */
    public function get_denda(Request $request)
    {
        $setting = Setting::first();
        $transaction = Transaction::find($request->id);
        $transaction->denda = 0;
        if ( date_create() > date_create($transaction->tanggal_tempo) ) {
            $diff = date_diff(date_create($transaction->tanggal_tempo), date_create());
            $transaction->denda = $diff->days * $setting->denda;
            $transaction->terlambat = (int) $diff->days;
        }

        return $transaction;
    }

    /**
     * get buku
     */
    public function get_buku(Request $request)
    {
        $transaction = Transaction::with('books')->where('id', $request->id)->first();
        $tags = '';
        foreach ($transaction->books as $book) {
            $tags .= '
                <tr>
                    <td>' . $book->kd_buku . '</td>
                    <td>' . $book->judul . '</td>
                    <td>' . $book->isbn . '</td>
                    <td>' . $book->pengarang . '</td>
                    <td>' . $book->pivot->qty . '</td>
                </tr>
            ';
        }
        return $tags;
    }
}
