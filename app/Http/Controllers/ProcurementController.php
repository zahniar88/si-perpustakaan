<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Procurement;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class ProcurementController extends Controller
{

    public function __construct() 
    {
        $this->middleware('AdminPetugas');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pengadaan.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $procurements = Procurement::with('book')->orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($procurements as $procurement) {
            $data[] = [
                'tanggal' => $procurement->tanggal,
                'judul' => $procurement->book->judul,
                'asal' => $procurement->asal,
                'jumlah' => $procurement->jumlah,
                'keterangan' => $procurement->keterangan,
                'opsi' => '
                    <form action="/pengadaan/' . $procurement->id . '" method="post">
                        ' . csrf_field() . method_field('DELETE') . '
                        <button class="btn btn-danger btn-sm rounded-0" btn-delete>
                            <i class="fas fa-trash-alt"></i> Hapus
                        </button>
                    </form>
                ',
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = Book::all();
        return view('admin.pengadaan.create', compact('books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'buku' => ['required', 'exists:books,id'],
            'asal' => ['required', 'string', 'max:100'],
            'jumlah' => ['required', 'numeric', 'digits_between:1,11'],
            'keterangan' => ['required', 'string', 'max:350']
        ]);

        $book = Book::find($request->buku);
        $procurement = $book->procurements()->create([
            'asal' => strip_tags($request->asal),
            'jumlah' => strip_tags($request->jumlah),
            'keterangan' => strip_tags($request->keterangan),
            'tanggal' => now()->isoFormat('DD-MM-YYYY')
        ]);

        if ( $procurement ) {
            $book->update([
                'jumlah' => $book->jumlah + $request->jumlah
            ]);
            return redirect('/pengadaan')->with('response', Notify::success('Data Pengadaan berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data Pengadaan gagal ditambahkan'))->withInput();
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $procurement = Procurement::findOrFail($id);
        $book = Book::find($procurement->book_id);
        $jumlah = $procurement->jumlah;

        if ( $procurement->delete() ) {
            $book->update([
                'jumlah' => $book->jumlah-$jumlah
            ]);
            return back()->with('response', Notify::success('Data berhasil dihapus'));
        } else {
            return back()->with('response', Notify::error('Data gagal dihapus'));
        }
        
    }
}
