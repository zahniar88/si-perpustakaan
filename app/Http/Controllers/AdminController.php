<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class AdminController extends Controller
{

    public function __construct() 
    {
        $this->middleware('Admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pengguna.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $users = User::with('role')->whereHas('role', function($query) {
            $query->where('level', '!=', 'Admin');
        })->orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($users as $user) {
            $status = '';
            if ( $user->status == 0 ) {
                $status = '
                    <span class="text-danger">
                        <i class="fas fa-info-circle text-danger"></i> Nonaktif
                    </span>
                ';
            } else if ($user->status == 1) {
                $status = '
                    <span class="text-success">
                        <i class="fas fa-check-circle text-success"></i> Aktif
                    </span>
                ';
            }
            

            $data[] = [
                'nama' => strip_tags($user->nama),
                'email' => $user->email,
                'username' => $user->username,
                'status' => $status,
                'level' => $user->role->level,
                'opsi' => '
                    <form action="/pengguna/' . $user->id . '" method="post">
                        ' . csrf_field() . method_field('DELETE') . '
                        <a href="/pengguna/' . $user->id . '/edit" class="btn btn-primary btn-sm rounded-0">
                            <i class="fas fa-edit"></i> Ubah
                        </a>
                        <button class="btn btn-danger btn-sm rounded-0" btn-delete>
                            <i class="fas fa-trash-alt"></i> Hapus
                        </button>
                    </form>
                ',
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.pengguna.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'nama' => ['required', 'max:30', 'string'],
            'email' => ['required', 'max:255', 'email', 'unique:users,email'],
            'username' => ['required', 'max:40', 'alpha_num', 'unique:users,username'],
            'level' => ['required', 'exists:roles,id'],
            'password' => ['required', 'min:8', 'confirmed']
        ]);

        $user = User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'role_id' => $request->level,
            'status' => 0
        ]);

        if ( $user ) {
            return redirect('/pengguna')->with('response', Notify::success('Data pengguna berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data pengguna gagal ditambahkan'))->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        if ( $user->role->level == 'Admin' ) {
            return back()->with('response', Notify::warning('Anda tidak dapat mengubah data admin'));
        }
        return view('admin.pengguna.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi
        $request->validate([
            'nama' => ['required', 'max:30', 'string'],
            'email' => ['required', 'max:255', 'email', 'unique:users,email,'.$id],
            'username' => ['required', 'max:40', 'alpha_num', 'unique:users,username,'.$id],
            'level' => ['required', 'exists:roles,id'],
            'status' => ['required', 'in:0,1']
        ]);

        $userFind = User::findOrFail($id);
        if ( $userFind->role->level == 'Admin' ) {
            return redirect('/pengguna')->with('response', Notify::error('Anda tidak dapat mengubah data admin'));
        }

        $user = $userFind->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'username' => $request->username,
            'role_id' => $request->level,
            'status' => $request->status
        ]);

        if ($user) {
            return redirect('/pengguna')->with('response', Notify::success('Data pengguna berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data pengguna gagal ditambahkan'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->doesntHave('transactions')->first();
        if ( $user ) {

            if ($user->role->level == 'Admin') {
                return redirect('/pengguna')->with('response', Notify::error('Anda tidak dapat menghapus data admin'));
            }

            if ($user->delete()) {
                return back()->with('response', Notify::success('Data berhasil dihapus'));
            } else {
                return back()->with('response', Notify::error('Data gagal dihapus'));
            }

        } else {
            return back()->with('response', Notify::error('Gagal, pengguna ini telah menginput transaksi'));
        }
        
    }
}
