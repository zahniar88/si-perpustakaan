<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Setting;
use App\Models\Student;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Space\Sweetalert\Notify;

class BorrowController extends Controller
{

    public function __construct() 
    {
        $this->middleware('AdminPetugas');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.peminjaman.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ajax()
    {
        $transactions = Transaction::with('student')->orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($transactions as $transaction) {
            $status = '';
            if ( $transaction->status == 0 ) {
                $status = '
                    <span class="badge badge-success font-weight-normal" style="font-size: 14px">
                        Kembali
                    </span>
                ';
            } else if ( $transaction->status == 1 ) {
                $status = '
                    <span class="badge badge-warning font-weight-normal" style="font-size: 14px">
                        Pinjam
                    </span>
                ';
            }

            // check late
            $late = 0;
            $tempo = $transaction->tanggal_kembali ?? $transaction->tanggal_tempo;
            $diff = date_diff(date_create($tempo), date_create());
            if ( date_create($transaction->tanggal_kembali) > date_create($tempo) ) {
                $late = (int) $diff->days;
            }
            

            $data[] = [
                'id' => $transaction->kd_pinjam,
                'tanggal_pinjam' => $transaction->tanggal_pinjam,
                'nama_siswa' => $transaction->student->nama,
                'tempo' => $transaction->tanggal_tempo,
                'terlambat' => $late . ' Hari',
                'status' => $status,
                'keterangan' => $transaction->keterangan,
                'opsi' => '
                    <a href="/peminjaman/' . $transaction->kd_pinjam . '" class="btn btn-primary btn-sm rounded-0">
                        <i class="fas fa-eye"></i> Detail
                    </a>
                ',
            ];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transaction = new Transaction();
        $setting = Setting::first();

        $id = $transaction->getCode();
        $lama_pinjam = $setting->lama_pinjam;
        $students = Student::all();
        $books = Book::where('jumlah', '!=', 0)->get();

        return view('admin.peminjaman.create', compact('students', 'books', 'id', 'lama_pinjam'));
    }

    /**
     * For Search book
     */
    public function find_book(Request $request)
    {
        $book = Book::find($request->id);

        return $book;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'siswa' => ['required', 'exists:students,id'],
            'buku' => ['required', 'max:3'],
            'buku.*' => ['required', 'exists:books,id', 'distinct'],
            'qty' => ['required'],
            'qty.*' => ['required', 'numeric'],
            'keterangan' => ['required', 'string', 'max:350']
        ]);
        
        $transaction = Transaction::create([
            'kd_pinjam' => $request->id,
            'tanggal_pinjam' => $request->tgl_pinjam,
            'tanggal_tempo' => $request->tgl_kembali,
            'keterangan' => $request->keterangan,
            'terlambat' => 0,
            'status' => 1,
            'user_id' => auth()->user()->id,
            'student_id' => $request->siswa
        ]);

        if ( $transaction ) {
            
            for ($i=0; $i < count($request->buku); $i++) { 
                $transaction->books()->attach($request->buku[$i], ['qty' => $request->qty[$i]]);
            }
            return redirect('/peminjaman')->with('response', Notify::success('Data peminjaman berhasil ditambahkan'));
        } else {
            return back()->with('response', Notify::error('Data peminjaman gagal ditambahkan'))->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kd_pinjam)
    {
        $transaction = Transaction::with('books')->where('kd_pinjam', $kd_pinjam)->firstOrFail();
        return view('admin.peminjaman.show', compact('transaction'));
    }
}
