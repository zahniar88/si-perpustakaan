<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    
    public function index()
    {
        return view('home');
    }

    public function index_ajax(Request $request)
    {
        if ( !empty($request->search) ) {
            $books = Book::with('rack', 'transactions', 'category')
                            ->where('judul', 'LIKE', '%'.$request->search.'%')->orderBy('id', 'DESC')
                            ->paginate(8);
        } else {
            $books = Book::with('rack', 'transactions', 'category')->orderBy('id', 'DESC')->paginate(8);
        }

        $html = $this->htmlData($request, $books);

        return $html;
    }

    public function htmlData(Request $request, $books)
    {
        $html = '';
        $pagination = '';
        if ( count($books) > 0 ) {
            foreach ($books as $book) {

                $totalSisa = $book->jumlah;
                $qty = 0;
                foreach ($book->transactions()->where('status', 1)->get() as $transaction) {
                    $qty += $transaction->pivot->qty;
                    $totalSisa = $book->jumlah - $qty;
                }

                $html .= '
                    <div class="col-md-6 mb-3">
                        <div class="card rounded-0 h-100">
                            <div class="card-body rounded-0">
                                <h5 class="card-title mb-0 text-primary">' . $book->judul . '</h5>
                                <p class="card-text mb-0">' . $book->sinopsis . '</p>
                                <hr>
                                <small class="text-dark">
                                    <b>Pengarang : </b> ' . $book->pengarang . '
                                </small> | 
                                <small class="text-dark">
                                    <b>Lokasi Buku : </b> ' . $book->rack->nama . '
                                </small> | 
                                <small class="text-dark">
                                    <b>Tahun terbit : </b> ' . $book->thn_terbit . '
                                </small> | 
                                <small class="text-dark">
                                    <b>Stok Buku : </b> ' . $totalSisa . '
                                </small> | 
                                <span class="badge badge-primary font-weight-normal rounded-0 p-1">
                                    <i class="fas fa-tags"></i> ' . $book->category->nama . '
                                </span>
                            </div>
                        </div>
                    </div>
                ';
            }
        } else {
            $html .= '
                <div class="col-md-12">
                    <div class="card rounded-0 py-5">
                        <div class="card-body rounded-0">
                            <h3 class="text-center card-title mb-0">
                                Buku tidak tersedia diperpustakaan
                            </h3>
                        </div>
                    </div>
                </div>
            ';
        }
        
        $pagination .= $books->onEachSide(1)->appends($request->input())->links();

        $data = ['html' => $html, 'pagination' => $pagination];

        return $data;
    }
}
