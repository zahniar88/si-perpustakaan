<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Procurement;
use App\Models\Student;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    
    /**
     * Dashboard view
     */
    public function index()
    {
        // counting
        $student = Student::count();
        $book = Book::count();
        $procurement = Procurement::count();
        $transaction = Transaction::where('status', 1)->count();

        // data
        $books = Book::with('transactions')->whereHas('transactions')->withCount('transactions')->limit(4)->get();
        $transactions = Transaction::with('student')->where('status', 1)->orderBy('id', 'DESC')->limit(5)->get();
        return view('admin.dashboard', compact('student', 'book', 'procurement', 'transaction', 'books', 'transactions'));
    }
}
