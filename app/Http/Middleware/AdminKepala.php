<?php

namespace App\Http\Middleware;

use Closure;

class AdminKepala
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->admin() && !auth()->user()->kepala()) {
            abort(403);
        }

        return $next($request);
    }
}
