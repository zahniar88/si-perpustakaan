<?php

namespace App\Http\Middleware;

use Closure;
use Space\Sweetalert\Notify;

class Activate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( auth()->user()->status != 1 ) {
            auth()->guard()->logout();
            return redirect('/login')->with('response', Notify::error('Status akun anda tidak aktif'));
        }
        
        return $next($request);
    }
}
