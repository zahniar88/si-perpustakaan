<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    // set kode penerbit
    public function getCode()
    {
        $publisher = $this->orderBy('kd_penerbit', 'DESC')->first();
        if ($publisher) {
            $num = (int) substr($publisher->kd_penerbit, 1, 3);
            $kode = 'P' . str_pad($num + 1, 3, 0, STR_PAD_LEFT);
        } else {
            $kode = 'P001';
        }

        return $kode;
    }

    public function books()
    {
        return $this->hasMany('App\Models\Book');
    }
}
