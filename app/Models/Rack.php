<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function books()
    {
        return $this->hasMany('App\Models\Book');
    }
}
