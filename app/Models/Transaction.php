<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function books()
    {
        return $this->belongsToMany('App\Models\Book')->withPivot('qty');
    }

    public function getCode()
    {
        $transaction = $this->orderBy('kd_pinjam', 'DESC')->first();
        if ( $transaction ) {
            $num = (int) substr($transaction->kd_pinjam, 2, 6);
            $kode = 'PJ' . str_pad($num+1, 6, 0, STR_PAD_LEFT);
        } else {
            $kode = 'PJ000001';
        }

        return $kode;
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
