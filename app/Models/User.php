<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function admin()
    {
        return $this->role->level == 'Admin';
    }

    public function petugas()
    {
        return $this->role->level == 'Petugas';
    }

    public function kepala()
    {
        return $this->role->level == 'Kepala';
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }
}
