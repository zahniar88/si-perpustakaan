<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    // get code
    public function getCode()
    {
        $book = $this->orderBy('kd_buku', 'DESC')->first();
        if ( $book ) {
            $num = (int) substr($book->kd_buku, 2, 4);
            $kode = 'BK' . str_pad($num+1, 4, 0, STR_PAD_LEFT);
        } else {
            $kode = 'BK0001';
        }
        
        return $kode;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function publisher()
    {
        return $this->belongsTo('App\Models\Publisher');
    }

    public function rack()
    {
        return $this->belongsTo('App\Models\Rack');
    }

    public function procurements()
    {
        return $this->hasMany('App\Models\Procurement');
    }

    public function transactions()
    {
        return $this->belongsToMany('App\Models\Transaction')->withPivot('qty');
    }
}
