<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    // set kode kategori
    public function getCode()
    {
        $category = $this->orderBy('kd_kategori', 'DESC')->first();
        if ( $category ) {
            $num = (int) substr($category->kd_kategori, 1, 3);
            $kode = 'K' . str_pad($num+1, 3, 0, STR_PAD_LEFT);
        } else {
            $kode = 'K001';
        }
        
        return $kode;
    }

    public function books()
    {
        return $this->hasMany('App\Models\Book');
    }
}
