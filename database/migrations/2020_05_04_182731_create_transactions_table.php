<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('kd_pinjam');
            $table->string('tanggal_pinjam');
            $table->string('tanggal_tempo');
            $table->string('tanggal_kembali')->nullable();
            $table->text('keterangan', 350);
            $table->integer('terlambat');
            $table->boolean('status');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
