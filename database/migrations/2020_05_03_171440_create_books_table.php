<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('kd_buku')->unique();
            $table->string('judul');
            $table->string('isbn');
            $table->string('pengarang');
            $table->integer('halaman');
            $table->integer('jumlah');
            $table->string('thn_terbit', 5);
            $table->text('sinopsis');
            
            // field relasi
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('publisher_id')->unsigned();
            $table->bigInteger('rack_id')->unsigned();

            // create realsi
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('publisher_id')->references('id')->on('publishers')->onDelete('cascade');
            $table->foreign('rack_id')->references('id')->on('racks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
