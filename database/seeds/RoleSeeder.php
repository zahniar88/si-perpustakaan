<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ( Role::count() == 0 ) {
            Role::insert([
                ['level' => 'Admin'],
                ['level' => 'Petugas'],
                ['level' => 'Kepala'],
            ]);
        }
    }
}
