<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class StudentFakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 10; $i++) {
            DB::table('students')->insert([
                'nis' => $faker->ean8,
                'nama' => $faker->name,
                'jk' => $faker->randomElement(['L', 'P']),
                'alamat' => $faker->streetAddress
            ]);
        }
    }
}
