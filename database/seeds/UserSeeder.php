<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ( User::count() == 0 ) {
            $role = Role::where('level', 'Admin')->first();
            $role->users()->create([
                'nama' => 'Admin',
                'email' => 'admin@email.com',
                'username' => 'admin',
                'password' => bcrypt('admin'),
                'status' => 1
            ]);
        }
    }
}
