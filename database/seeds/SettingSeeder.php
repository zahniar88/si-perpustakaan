<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ( Setting::count() == 0 ) {
            Setting::create([
                'denda' => 3000,
                'lama_pinjam' => 3
            ]);
        }
    }
}
