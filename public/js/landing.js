$(document).ready(function(){

    // ajax
    var ajax_data = function(url) {
        $.ajax({
            method: 'POST',
            url: url,
            beforeSend: function() {
                var html = '';
                html = `
                    <div class="col-md-12">
                        <div class="card rounded-0 py-2">
                            <div class="card-body rounded-0 text-center">
                                <span class="spinner-grow spinner-grow-lg text-primary" role="status" aria-hidden="true" style="width: 4rem; height: 4rem;"></span>
                                <span class="d-block mt-4 text-primary" style="font-size: 23px">Memuat</span>
                            </div>
                        </div>
                    </div>
                `;
                $('#content').html(html);
            }
        }).done(function (data) {
            $('#content').html(data['html']);
            $('#pagination').html(data['pagination']);
            $(window).scrollTop(0);
        });
    }

    ajax_data('/data-landing');

    // pagination click function
    $('body').on('click', '.pagination .page-link', function(e) {
        e.preventDefault();
        ajax_data($(this).attr('href'));
    });

    // form search function
    $('#filter').submit(function(e){
        e.preventDefault();
        var search = $('#search').val();
        var url = '/data-landing?search=' + search;
        ajax_data(url);
    });

});