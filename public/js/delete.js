$('body').on('click', 'td button[btn-delete]', function(e) {
    e.preventDefault();
    var target = $(this).parent('form');
    Swal.fire({
        title: 'Peringatan',
        text: "Yakin ingin hapus data?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Oke'
    }).then((result) => {
        if (result.value) {
            target.submit();
        }
    });
});